import redis
import os	
from flask import Flask

app = Flask(__name__)
redis = redis.Redis(host='redis', port=6379, db=0)

@app.route('/hello')
def hello():
    return os.getenv('NAME')
